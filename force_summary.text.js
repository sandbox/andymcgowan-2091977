(function ($) {
  /**
   * Don't hide summary
   */
  Drupal.behaviors.textSummary = {
    attach: function (context, settings) {
      $('.text-summary', context).once('text-summary', function () {
        var $widget = $(this).closest('div.field-type-text-with-summary');
        var $summaries = $widget.find('div.text-summary-wrapper');

        $summaries.once('text-summary-wrapper').each(function(index) {
          var $summary = $(this);

          var $summaryLabel = $summary.find('label');
          var $full = $widget.find('.text-full').eq(index).closest('.form-item');
          var $fullLabel = $full.find('label');

          var maxLength = Drupal.settings.force_summary.maxLength;

          $summary.find('.summary-maxchars').html('(<span class="summary-remaining">' + maxLength + '</span> characters remaining)');

          $summary.find('.text-summary').bind('input propertychange', function(e) {
            var len = e.target.value.length;
            var remaining = maxLength - len;
            $summary.find('.summary-remaining').html(remaining);
            if(len > maxLength){
              $summary.find('.summary-remaining').addClass('toolong');
            }else{
              $summary.find('.summary-remaining').removeClass('toolong');
            }
          });
          // Create a placeholder label when the field cardinality is
          // unlimited or greater than 1.
          if ($fullLabel.length == 0) {
            $fullLabel = $('<label></label>').prependTo($full);
          }
          return;
        });
      });
    }
  };
})(jQuery);
